﻿using System;
using System.Diagnostics;
using System.IO;

namespace CommitPolicys
{
    class Program
    {
        //static bool gonext = true;
        static void Main(string[] args)
        {
            string currentPath = Directory.GetCurrentDirectory();
            string inputPath = currentPath + "\\inputs.txt";
            if (!File.Exists(inputPath)) { Console.WriteLine("inputs.txt not found!!!"); return; }

            ProcessStartInfo startinfo = new ProcessStartInfo
            {
                FileName = @"c:\\windows\\system32\\cmd.exe",
                //Arguments = "/c \"dir /s\"",
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false
            };

            Process process = new Process();
            process.StartInfo = startinfo;

            process.OutputDataReceived += process_DataReceived;
            process.ErrorDataReceived += process_DataReceived;
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();



            string[] lines = File.ReadAllLines(inputPath);

            int lineNo = 0;
            foreach (string line in lines)
            {
                //while (gonext == false)
                //    continue;

                process.StandardInput.Flush();

                string[] values = line.Split('|'); // com|path|name|value
                if (!IsValidPath(inputPath))
                {
                    Console.WriteLine($"Invlaid input at line {lineNo}");
                    lineNo++;
                    continue;
                }

                switch (values[0])
                {
                    case "add":
                    case "set":
                        {
                            process.StandardInput.WriteLine($"reg add {values[1]} /v {values[2]} /t {values[3]} /d {values[4]} /f");
                            //gonext = false;
                            break;
                        }
                    case "del":
                        {
                            break;
                        }
                    case "cpd":
                    case "cpf":
                        {
                            string command = $"xcopy .\\{values[1]} \"{values[2]}\" /e /d";
                            process.StandardInput.WriteLine(command);
                            //gonext = false;
                            break;
                        }
                }


                lineNo++;
            }

            Console.ReadKey();
        }

        private static void process_DataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
            //gonext = trkue;
        }

        private static bool IsValidPath(string path)
        {
            return true;
        }
    }
}
